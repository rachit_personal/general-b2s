package com.app.generalb2s.seller.service;

import com.app.generalb2s.buyer.entity.BuyerAuthEntity;
import com.app.generalb2s.buyer.repository.BuyerAuthRepo;
import com.app.generalb2s.common.entity.AuthEntity;
import com.app.generalb2s.common.repository.AuthRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class SellerValidationService {

    @Autowired
    private AuthRepo repo;

    public boolean validate(String tokenId, String deviceId) {

        Calendar cal = Calendar.getInstance();
        List<AuthEntity> authEntity = repo.checkAuth(deviceId, tokenId, cal.getTimeInMillis());

        return !authEntity.isEmpty() ? true : false;


    }


}
