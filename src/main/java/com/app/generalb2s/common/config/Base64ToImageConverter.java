package com.app.generalb2s.common.config;

import javax.xml.bind.DatatypeConverter;
import java.awt.image.ImagingOpException;
import java.io.*;

public class Base64ToImageConverter {

    public static File getImageFromBase64(String base64String, String fileName) {
        String[] strings = base64String.split(",");
        String extension = "";
        switch (strings[0]) { // check image's extension
            case "data:image/jpeg;base64":
                //extension = "jpeg";
                break;
            case "data:image/png;base64":
                //extension = "png";
                break;
            case "data:image/jpg;base64":
                //extension = "jpg";
                break;
            default: // should write cases for more images types
                extension = "jpg";
                break;
        }
        // convert base64 string to binary data
        byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
        File file = new File(fileName + extension);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (ImagingOpException | IOException e) {
            e.printStackTrace();
        }
        return file;
    }


}
