package com.app.generalb2s.seller.service;

import com.app.generalb2s.buyer.entity.BuyerAuthEntity;
import com.app.generalb2s.buyer.repository.BuyerAuthRepo;
import com.app.generalb2s.common.entity.AuthEntity;
import com.app.generalb2s.common.repository.AuthRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class BuyerValidationService {

    @Autowired
    private BuyerAuthRepo repo;

    public boolean validate(String deviceId, String tokenId) {

        Calendar cal = Calendar.getInstance();
        List<BuyerAuthEntity> authEntity = repo.checkAuth(deviceId, tokenId, cal.getTimeInMillis());

        return !authEntity.isEmpty() ? true : false;


    }


}
