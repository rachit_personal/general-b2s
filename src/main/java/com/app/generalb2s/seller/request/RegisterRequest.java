package com.app.generalb2s.seller.request;

import com.app.generalb2s.common.enums.CategoryEnum;
import com.app.generalb2s.seller.enums.SellerAccountStatus;

import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.lang.NonNull;

public class RegisterRequest {

    @NotNull
    private Long shopId;

    @NotNull(message = "shopname is empty")
    private String shopName;
    @NotNull(message = "ownerName is empty")
    private String ownerName;

    private SellerAccountStatus status;
    
    @NotNull(message = "lat is empty")
    private String latitude;
    
    @NotNull(message = "long is empty")
    private String longitude;
    private String emailId;

    @NotNull(message = "Phone is empty")
    @Pattern(regexp = "^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$" , message = "invalid phone number... format +919876543210")
    private String mobNo;

    @NotNull(message = "Password is empty")
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", message = "Password should be Minimum eight characters, at least one letter and one number")
    private String password;
    private String landmark;	

    @NotNull(message = "Pincode is empty")
    private String pincode;
    
    @NotNull(message = "Category is empty")
    private String categoryId;
       

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}


    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public SellerAccountStatus getStatus() {
        return status;
    }

    public void setStatus(SellerAccountStatus status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
