package com.app.generalb2s.seller.response;

public class ServiceResponse {

    private Response serviceStatus;
    private String serviceMessage;

    public Response getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(Response serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceMessage() {
        return serviceMessage;
    }

    public void setServiceMessage(String serviceMessage) {
        this.serviceMessage = serviceMessage;
    }


}
