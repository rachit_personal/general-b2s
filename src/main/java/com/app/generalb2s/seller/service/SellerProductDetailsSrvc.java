package com.app.generalb2s.seller.service;

import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.seller.request.ProductDetailsByIdRequest;
import com.app.generalb2s.seller.request.ProductDetailsRequest;
import com.app.generalb2s.seller.response.ProductCatogories;
import com.app.generalb2s.seller.response.ProductDetails;
import com.app.generalb2s.seller.response.ProductDetailsResponse;
import com.app.generalb2s.seller.response.SellerDetailsResponse;

public interface SellerProductDetailsSrvc {

    ProductDetailsResponse saveSellerProductDetails(ProductDetailsRequest productDetailsRequest);

    ProductDetails getSellerProductDetails(ProductDetailsByIdRequest productDetailsRequest) throws ProductNotFoundException;

    ProductDetailsResponse updateSellerProductDetails(ProductDetailsRequest productDetailsRequest, String productId) throws ProductNotFoundException;

    ProductDetailsResponse deleteSellerProductDetails(String productId) throws ProductNotFoundException;

    SellerDetailsResponse getShopDetailsByShopId(String shopId);
}
