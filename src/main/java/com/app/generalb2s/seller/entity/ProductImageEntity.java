package com.app.generalb2s.seller.entity;

import javax.persistence.*;

@Entity
@Table(name = "product_image_details")
public class ProductImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "imageId")
    private Integer imageId;
    private String imageS3Path;
    private String categoryId;
    private Long productId;
    private String imageName;


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getImageS3Path() {
        return imageS3Path;
    }

    public void setImageS3Path(String imageS3Path) {
        this.imageS3Path = imageS3Path;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ProductImageEntity [imageId=" + imageId + ", imageS3Path=" + imageS3Path + ", categoryId=" + categoryId
                + ", productId=" + productId + ", imageName=" + imageName + "]";
    }

}
