package com.app.generalb2s.buyer.repository;

import com.app.generalb2s.buyer.entity.BuyerAuthEntity;
import com.app.generalb2s.common.entity.AuthEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BuyerAuthRepo extends JpaRepository<BuyerAuthEntity, Long> {

    @Query(value = "SELECT s FROM BuyerAuthEntity s WHERE s.deviceId = ?1 and s.tokenId = ?2 and s.status='ACTIVE' and"
            + " s.sessionExpiry > ?3")
    List<BuyerAuthEntity> checkAuth(String deviceId, String loginId, long time);

}
