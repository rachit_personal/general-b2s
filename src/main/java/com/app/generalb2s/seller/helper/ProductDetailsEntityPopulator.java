package com.app.generalb2s.seller.helper;

import com.app.generalb2s.seller.entity.ProductDetailsRequestEntity;
import com.app.generalb2s.seller.entity.ProductImageEntity;
import com.app.generalb2s.seller.request.ProductDetailsRequest;
import com.app.generalb2s.seller.response.ProductDetails;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class ProductDetailsEntityPopulator {


    public static ProductDetailsRequestEntity getProductDetailsEntityObj(ProductDetailsRequest pd) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(pd, ProductDetailsRequestEntity.class);
    }

    public static ProductImageEntity populateImageDetails(String imageS3UrlPath, ProductDetailsRequestEntity productDetailsRequestEntity) {

        ProductImageEntity productImageEntity = new ProductImageEntity();
        productImageEntity.setImageS3Path(imageS3UrlPath);
        productImageEntity.setCategoryId(productDetailsRequestEntity.getCategoryId());
        productImageEntity.setProductId(productDetailsRequestEntity.getProductId());
        productImageEntity.setImageName(imageS3UrlPath.substring(imageS3UrlPath.lastIndexOf('_') + 1).trim());
        return productImageEntity;
    }

    public static ProductDetails getProductDetailsEntityObj(ProductDetailsRequestEntity p) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(p, ProductDetails.class);
    }

    public static ProductDetailsRequestEntity getProductDetailsEntityObjForUpdate(ProductDetailsRequestEntity productDetailsEntity, ProductDetailsRequest productDetailsRequest) {
        productDetailsEntity.setProductDesc(productDetailsRequest.getProductDesc());
        productDetailsEntity.setProductBrand(productDetailsRequest.getProductBrand());
        productDetailsEntity.setProductName(productDetailsRequest.getProductName());
        productDetailsEntity.setCategoryId(productDetailsRequest.getCategoryId());
        productDetailsEntity.setSubCategoryId(productDetailsRequest.getSubCategoryId());
        productDetailsEntity.setProductPrice(productDetailsRequest.getProductPrice());
        return productDetailsEntity;
    }
}
