package com.app.generalb2s.common.exception;

public class InvalidMobileNumber extends Exception{

    public InvalidMobileNumber(String msg){
        super(msg);
    }
}
