package com.app.generalb2s.seller.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.generalb2s.seller.entity.ProductCategories;
import com.app.generalb2s.seller.repository.ProductCategoryRepo;

@Service
public class SellerCategoriesService {

	@Autowired
	private ProductCategoryRepo repo;
	
	public List<ProductCategories> getCategories() {
		return repo.findAll();
	}
}
