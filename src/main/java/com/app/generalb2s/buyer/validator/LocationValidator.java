package com.app.generalb2s.buyer.validator;

import static java.util.Objects.nonNull;

public class LocationValidator {

    public static boolean validateLocationParameter(String lat, String lon){
        if (nonNull(lat) && nonNull(lon) && !lat.isEmpty() && !lon.isEmpty()){
            return true;
        }
        else
            return false;
    }
}
