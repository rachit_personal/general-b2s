package com.app.generalb2s.common.exception;

public class InvalidLocationDetailsException extends Exception{

    public InvalidLocationDetailsException(String message){
        super(message);
    }
}
