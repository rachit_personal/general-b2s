package com.app.generalb2s.buyer.response;

import com.app.generalb2s.seller.response.ServiceResponse;

public class BuyerRegisterResponse extends ServiceResponse {

    private String buyerId;
    private String message;

    
    
    public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
