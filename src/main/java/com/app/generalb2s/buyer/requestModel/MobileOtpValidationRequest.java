package com.app.generalb2s.buyer.requestModel;

public class MobileOtpValidationRequest {

    private String mobNo;
    private String otp;

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
