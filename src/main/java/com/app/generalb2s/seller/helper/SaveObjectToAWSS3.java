package com.app.generalb2s.seller.helper;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import com.app.generalb2s.common.config.Base64ToImageConverter;
import com.app.generalb2s.common.model.ImageDetails;
import com.app.generalb2s.seller.request.ProductDetailsRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
@Slf4j
public class SaveObjectToAWSS3 {

    @Autowired
    AmazonS3 amazonS3;

    @Value("${bucket.seller}")
    private String bucketName;

    private String uploadFileToS3Bucket(File file, ProductDetailsRequest productDetailsRequest, Long productId) {
        final String uniqueFileName = LocalDateTime.now() + "_" + productDetailsRequest.getCategoryId() + "_" + productId + "_" + file.getName();
        //LOGGER.info("Uploading file with name= " + uniqueFileName);
        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, uniqueFileName, file);
        amazonS3.putObject(putObjectRequest);
        file.delete();
        return uniqueFileName;
    }

    public String fetchFileToS3Bucket(String name) throws IOException {
        //LOGGER.info("Uploading file with name= " + uniqueFileName);
        final GetObjectRequest putObjectRequest = new GetObjectRequest(bucketName, name);
        try (S3ObjectInputStream is = amazonS3.getObject(putObjectRequest).getObjectContent()) {
            File tmp = File.createTempFile("s3test", "");
            IOUtils.copy(is, new FileOutputStream(tmp));
            byte[] fileContent = java.nio.file.Files.readAllBytes(tmp.toPath());
            return Base64.getEncoder().encodeToString(fileContent);
        }
    }


    public List<String> saveProductDetailsToS3(ProductDetailsRequest productDetailsRequest, Long productId) {

        List<String> productImageStoredS3Url = productDetailsRequest.getProductImage().stream().filter(x -> nonNull(x)).map(d -> uploadTOS3(d, productDetailsRequest, productId)).collect(Collectors.toList());

        return productImageStoredS3Url;
    }

    public String uploadTOS3(ImageDetails imageDetails, ProductDetailsRequest productDetailsRequest, Long productId) {
        try {
            if (nonNull(imageDetails)) {
                File fileToUpload = Base64ToImageConverter.getImageFromBase64(imageDetails.getBase64Image(), imageDetails.getImageName());
                return uploadFileToS3Bucket(fileToUpload, productDetailsRequest, productId);
            } else {
                throw new Exception("Base64 to image file conversion failed.");
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "Something went wrong with upload process";
        }
    }

    @Async
    public void deleteFileFromS3(final String keyName) {
        final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucketName, keyName);
        amazonS3.deleteObject(deleteObjectRequest);
    }
}
