package com.app.generalb2s.buyer.controller;

import com.app.generalb2s.buyer.requestModel.*;
import com.app.generalb2s.buyer.response.BuyerRegisterResponse;
import com.app.generalb2s.buyer.response.LoginResponse;
import com.app.generalb2s.buyer.service.BuyerService;
import com.app.generalb2s.buyer.validator.LocationValidator;
import com.app.generalb2s.common.exception.AlreadyRegisteredException;
import com.app.generalb2s.common.exception.InvalidLocationDetailsException;
import com.app.generalb2s.common.exception.InvalidMobileNumber;
import com.app.generalb2s.common.exception.InvalidOtpException;
import com.app.generalb2s.common.exception.InvalidTokenException;
import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.seller.request.LoginRequest;
import com.app.generalb2s.seller.service.BuyerValidationService;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.Service;
import com.twilio.rest.verify.v2.service.Verification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

@Controller
@RequestMapping(path = "/buyer/")
public class BuyerController {

    @Autowired
    BuyerService buyerService;

    @Autowired
    BuyerValidationService val;

    @GetMapping(path = "hello")
    public ResponseEntity<String> getHelloTest() {
        return new ResponseEntity<>("Hello World", HttpStatus.OK);
    }

    @PostMapping(path = "getDataByLocation")
    public ResponseEntity<Object> getDataByLocation(@RequestBody ProductsDetailsLocationRequest productsDetailsLocationRequest) throws InvalidTokenException, InvalidLocationDetailsException {
        if (!val.validate(productsDetailsLocationRequest.getDeviceId(), productsDetailsLocationRequest.getTokenId())) {
            throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
        }
        if (!LocationValidator.validateLocationParameter(productsDetailsLocationRequest.getLattitude(), productsDetailsLocationRequest.getLongitude())) {
            throw new InvalidLocationDetailsException("Invalid Lattitude/Longitude provided");
        }
        return new ResponseEntity<>(buyerService.getDataByLocation(productsDetailsLocationRequest), HttpStatus.OK);
    }

    @PostMapping(path = "/product/wishList/add")
    public ResponseEntity<Object> saveProductAsWishListed(@Valid @RequestBody WishListRequest wishListRequest) throws InvalidTokenException, ProductNotFoundException {
        if (!val.validate(wishListRequest.getDeviceId(), wishListRequest.getTokenId())) {
            throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
        } else {
            return new ResponseEntity<>(buyerService.saveWishListProduct(wishListRequest), HttpStatus.CREATED);
        }
    }

    @GetMapping(path = "/product/getWishList/{buyerId}")
    public ResponseEntity<Object> getProductAsWishList(@RequestBody RequestComponents requestComponents, @PathVariable String buyerId) throws InvalidTokenException, ProductNotFoundException {
        if (!val.validate(requestComponents.getDeviceId(), requestComponents.getTokenId())) {
            throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
        } else {
            return new ResponseEntity<>(buyerService.getWishListProduct(buyerId), HttpStatus.OK);
        }
    }


    @PostMapping(path = "getDataByLocation/{categoryId}")
    public ResponseEntity<Object> getDataByLocation(@RequestBody ProductsDetailsLocationRequest productsDetailsLocationRequest,
                                                    @PathVariable String categoryId) throws InvalidTokenException, InvalidLocationDetailsException {
        if (!val.validate(productsDetailsLocationRequest.getDeviceId(), productsDetailsLocationRequest.getTokenId())) {
            throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
        }
        if (!LocationValidator.validateLocationParameter(productsDetailsLocationRequest.getLattitude(), productsDetailsLocationRequest.getLongitude())) {
            throw new InvalidLocationDetailsException("Invalid Lattitude/Longitude provided");
        }
        return new ResponseEntity<>(buyerService.getDataByLocation(productsDetailsLocationRequest, categoryId), HttpStatus.OK);
    }


    @PostMapping(path = "buyerLogin")
    public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request) {

        if (request.getLoginId() != null && request.getPassword() != null) {
            return new ResponseEntity<>(buyerService.login(request), HttpStatus.OK);
        } else {
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setServiceMessage("Login username or password is missing or not provided.");
            return new ResponseEntity<>(null, HttpStatus.OK);
        }

    }

    @PostMapping(path = "buyerRegister")
    //return unique shop id
    public ResponseEntity<BuyerRegisterResponse> register(@RequestBody @Valid BuyerRegisterRequest request) throws InvalidOtpException, AlreadyRegisteredException {
    	//redepoly
        return new ResponseEntity<>(buyerService.register(request), HttpStatus.OK);

    }
    


    @PostMapping(path="generateOTP")
    public ResponseEntity<Object> generateOTP(@RequestBody BuyerRegisterRequest buyerRegisterRequest) throws InvalidMobileNumber {

        return new ResponseEntity<>(buyerService.generateOTP(buyerRegisterRequest), HttpStatus.OK);
    }

    @PostMapping(path="validateOtp")
    public ResponseEntity<Object> validateOtp(@RequestBody MobileOtpValidationRequest mobileOtpValidationRequest){
        return new ResponseEntity<>(buyerService.validateOtp(mobileOtpValidationRequest), HttpStatus.OK);
    }

    @GetMapping(path="validateExistingBuyer")
    public ResponseEntity<Object> validateExistingBuyer(@QueryParam("mobNo") String mobNo) throws InvalidMobileNumber {
        return new ResponseEntity<>(buyerService.validateExistingBuyer(mobNo), HttpStatus.OK);
    }


    @GetMapping(path="triggerOTP/")
    public ResponseEntity<Object> triggerOTP(@QueryParam("mobNo") String mobNo) throws InvalidMobileNumber {
        BuyerRegisterRequest buyerRegisterRequest= new BuyerRegisterRequest();
        buyerRegisterRequest.setMobNo(mobNo);
        return new ResponseEntity<>(buyerService.generateOTP(buyerRegisterRequest), HttpStatus.OK);
    }
}
