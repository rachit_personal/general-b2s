package com.app.generalb2s.buyer.requestModel;

import lombok.NonNull;

public class WishListRequest extends RequestComponents{
    @NonNull
    private String productId;

    @NonNull
    private String buyerId;

    private String isWishListed;

    public String getIsWishListed() {
        return isWishListed;
    }

    public void setIsWishListed(String isWishListed) {
        this.isWishListed = isWishListed;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }


}
