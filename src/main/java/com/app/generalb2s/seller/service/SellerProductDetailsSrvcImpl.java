package com.app.generalb2s.seller.service;

import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.common.model.ImageDetails;
import com.app.generalb2s.seller.entity.ProductDetailsRequestEntity;
import com.app.generalb2s.seller.entity.ProductImageEntity;
import com.app.generalb2s.seller.entity.RegisterEntity;
import com.app.generalb2s.seller.helper.ProductDetailsEntityPopulator;
import com.app.generalb2s.seller.helper.SaveObjectToAWSS3;
import com.app.generalb2s.seller.repository.ProductImageDao;
import com.app.generalb2s.seller.repository.SellerDetails;
import com.app.generalb2s.seller.repository.SellerProductDetailsDao;
import com.app.generalb2s.seller.request.ProductDetailsByIdRequest;
import com.app.generalb2s.seller.request.ProductDetailsRequest;
import com.app.generalb2s.seller.response.ProductDetails;
import com.app.generalb2s.seller.response.ProductDetailsResponse;
import com.app.generalb2s.seller.response.Response;
import com.app.generalb2s.seller.response.SellerDetailsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;


@Service
@Slf4j
public class SellerProductDetailsSrvcImpl implements SellerProductDetailsSrvc {

    @Autowired
    SellerProductDetailsDao sellerProductDetailsDao;

    @Autowired
    private SaveObjectToAWSS3 saveObjectToAWSS3;

    @Autowired
    ProductImageDao productImageDao;
    
    @Autowired
    SellerDetails seller;

    //@Transactional
    public ProductDetailsResponse saveSellerProductDetails(ProductDetailsRequest productDetailsRequest) {
        ProductDetailsResponse response = new ProductDetailsResponse();

        try {

            ProductDetailsRequestEntity productDetailsEntity = sellerProductDetailsDao.save(ProductDetailsEntityPopulator.getProductDetailsEntityObj(productDetailsRequest));

            if (nonNull(productDetailsEntity)) {
                productDetailsEntity = saveImageTOS3(productDetailsRequest, productDetailsEntity);
            }
            sellerProductDetailsDao.save(productDetailsEntity);

            if (nonNull(productDetailsEntity)) {
                response.setProductId(productDetailsEntity.getProductId().toString());
                response.setMessage("Data saved Successfully");
            }
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage("Something went wrong while saving product details");
            return response;
        }
    }

    @Override
    public ProductDetails getSellerProductDetails(ProductDetailsByIdRequest productDetailsRequest) throws ProductNotFoundException {
        ProductDetails productDetails = null;
        Optional<ProductDetailsRequestEntity> p = Optional.ofNullable(sellerProductDetailsDao.findById(Long.valueOf(productDetailsRequest.getProductId()))
                .orElseThrow(() -> new ProductNotFoundException("Product not found on :: " + productDetailsRequest.getProductId())));

        List<ImageDetails> imageDetails = new ArrayList<>();
        for (ProductImageEntity image : p.get().getpImage()) {
            ImageDetails e = new ImageDetails();
            e.setImageName(image.getImageName());
            try {
                String imageB64 = saveObjectToAWSS3.fetchFileToS3Bucket(image.getImageS3Path());
                e.setBase64Image(imageB64);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            imageDetails.add(e);
        }
        productDetails = ProductDetailsEntityPopulator.getProductDetailsEntityObj(p.get());
        productDetails.setProductImage(imageDetails);

        return productDetails;
    }


    public ProductDetailsResponse updateSellerProductDetails(ProductDetailsRequest productDetailsRequest, String productId) throws ProductNotFoundException {
        ProductDetailsResponse response = new ProductDetailsResponse();
        try {
            Optional<ProductDetailsRequestEntity> productDetailsEntity = Optional.ofNullable(sellerProductDetailsDao.findById(Long.valueOf(productId))
                    .orElseThrow(() -> new ProductNotFoundException("Product not found for :: " + productId)));
            productDetailsEntity.get().getpImage().stream().map(data -> {
                saveObjectToAWSS3.deleteFileFromS3(data.getImageS3Path());
                return null;
            });
            productImageDao.deleteImageBySellerId(Long.valueOf(productId));
            productDetailsEntity = Optional.ofNullable(saveImageTOS3(productDetailsRequest, productDetailsEntity.get()));
            productDetailsEntity = Optional.ofNullable(ProductDetailsEntityPopulator.getProductDetailsEntityObjForUpdate(productDetailsEntity.get(), productDetailsRequest));
            ProductDetailsRequestEntity entity = sellerProductDetailsDao.save(productDetailsEntity.get());
            if (nonNull(entity)) {
                response.setProductId(productDetailsEntity.get().getProductId().toString());
                response.setMessage("Data updated Successfully");
            }
            return response;
        } catch (Exception e) {
            throw e;
        }
    }

    public ProductDetailsResponse deleteSellerProductDetails(String productId) throws ProductNotFoundException {
        ProductDetailsResponse productDetailsResponse = new ProductDetailsResponse();
        productDetailsResponse.setProductId(productId);
        Optional<ProductDetailsRequestEntity> p = Optional.ofNullable(sellerProductDetailsDao.findById(Long.valueOf(productId))
                .orElseThrow(() -> new ProductNotFoundException("Product not found for :: " + productId)));
        try {
            p.get().getpImage().stream().map(data -> {
                saveObjectToAWSS3.deleteFileFromS3(data.getImageS3Path());
                return null;
            });
            sellerProductDetailsDao.deleteById(Long.valueOf(productId));
            productDetailsResponse.setServiceMessage("Product deleted successfully");
            productDetailsResponse.setServiceStatus(Response.SUCCESS);
        } catch (Exception e) {
            productDetailsResponse.setServiceMessage("Product deletion failed. Please try again");
            productDetailsResponse.setServiceStatus(Response.ERROR);
        }
        return productDetailsResponse;
    }

    public ProductDetailsRequestEntity saveImageTOS3(ProductDetailsRequest productDetailsRequest, ProductDetailsRequestEntity productDetailsRequestEntity) {
        List<String> imageS3UrlPath = null;
		try {
			imageS3UrlPath = saveObjectToAWSS3.saveProductDetailsToS3(productDetailsRequest, productDetailsRequestEntity.getProductId());
		} catch (Exception e) {
			e.printStackTrace();
		}
        List<ProductImageEntity> productImageEntities = new ArrayList<>();
        if (nonNull(imageS3UrlPath) && !imageS3UrlPath.isEmpty()) {
            productImageEntities = imageS3UrlPath.parallelStream().map(d ->
                    ProductDetailsEntityPopulator.populateImageDetails(d, productDetailsRequestEntity)
            ).collect(Collectors.toList());
        }
        if(!productImageEntities.isEmpty()) {
            productDetailsRequestEntity.setpImage(productImageEntities);
        }
        return productDetailsRequestEntity;
    }

    public SellerDetailsResponse getShopDetailsByShopId(String shopId){
        List<ProductDetails> productDetailsList= new ArrayList<>();
        List<ImageDetails> productImages= new ArrayList<>();
        SellerDetailsResponse sellerDetailsResponse= new SellerDetailsResponse();
        
        RegisterEntity re = seller.getById(Long.valueOf(shopId));
        sellerDetailsResponse.setCategoryid(re.getCategoryid());
        sellerDetailsResponse.setLatitude(re.getLatitude());
        sellerDetailsResponse.setLongitude(re.getLongitude());
        sellerDetailsResponse.setMobNo(re.getMobNo());
        sellerDetailsResponse.setOwnerName(re.getOwnerName());
        sellerDetailsResponse.setShopId(re.getShopId());
        sellerDetailsResponse.setShopName(re.getShopName());
        
        List<ProductDetailsRequestEntity> productDetailsRequestEntities= sellerProductDetailsDao.getProductDetailsByShopId(shopId);
        if(nonNull(productDetailsRequestEntities) && !productDetailsRequestEntities.isEmpty()){
            for (ProductDetailsRequestEntity productDetailsRequestEntity: productDetailsRequestEntities){
                if (!productDetailsRequestEntity.getpImage().isEmpty()){
                    for (ProductImageEntity productImageEntity: productDetailsRequestEntity.getpImage()){
                        ImageDetails e = new ImageDetails();
                        e.setImageName(productImageEntity.getImageName());
                        try {
                            String imageB64 = saveObjectToAWSS3.fetchFileToS3Bucket(productImageEntity.getImageS3Path());
                            e.setBase64Image(imageB64);
                        } catch (IOException e1) {

                            e1.printStackTrace();
                        }

                        productImages.add(e);
                    }
                }
                ProductDetails productDetails=ProductDetailsEntityPopulator.getProductDetailsEntityObj(productDetailsRequestEntity);
                productDetails.setProductImage(productImages);

                productDetailsList.add(productDetails);
            }
        }
        sellerDetailsResponse.setMessage("Product details fetch successfully.");
        sellerDetailsResponse.setProductDetailsList(productDetailsList);
        return sellerDetailsResponse;
    }

}