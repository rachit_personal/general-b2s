package com.app.generalb2s.seller.repository;

import com.app.generalb2s.seller.entity.RegisterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterDao extends JpaRepository<RegisterEntity, Integer> {

}
