package com.app.generalb2s.common.validator;

import com.app.generalb2s.seller.request.LoginRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static java.util.Objects.nonNull;

@Component
@Slf4j
public class LoginValidation {

    public boolean validation(LoginRequest request) {

        if (nonNull(request.getLoginId()) && !request.getLoginId().isEmpty() && nonNull(request.getPassword()) && !request.getPassword().isEmpty()) {

            return true;
        } else {
            return false;
        }
    }
}
