package com.app.generalb2s.seller.repository;

import com.app.generalb2s.seller.entity.RegisterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public  interface SellerDetails extends JpaRepository<RegisterEntity,Long> {

	@Query(value = "SELECT r FROM RegisterEntity r WHERE r.mobNo = ?1 or r.emailId = ?1")
    RegisterEntity checkLogin(String loginId);
/**
 * THis is to find data based on distance which is based on lat and long provided in Kms uptill 20 kms range.
 * To make it work for miles change 6371 to 3959
 */
	/*
	 * @Query(value= "Select r.seller_id, r.shopName," + " (" + "6371 * " +
	 * "acos(cos(radians(37)) * " + " cos(radians(?1)) *" + "cos(radians(?2) - " +
	 * "radians(-122)) + " + "sin(radians(37)) * " + "sin(radians(?1)))"+
	 * ") AS distance" +
	 * " FROM shop_details r HAVING distance < 5 ORDER BY distance LIMIT 0, 10 "
	 * ,nativeQuery = true)
	 */
	
	
	@Query(value= "SELECT *,(6371000 * Acos (Cos (Radians(?1)) * Cos(Radians(latitude)) * Cos(Radians(longitude) - Radians(?2)) + Sin (Radians(?1)) * Sin(Radians(latitude)))) AS distance_m FROM shop_details sd  HAVING distance_m < 5000 ORDER  BY distance_m",nativeQuery = true)
    List<RegisterEntity> getShopDetailsBasedOnLocation(String lattitude, String longitude);

	@Query(value= "SELECT *,(6371000 * Acos (Cos (Radians(?1)) * Cos(Radians(latitude)) * Cos(Radians(longitude) - Radians(?2)) + Sin (Radians(?1)) * Sin(Radians(latitude)))) AS distance_m FROM shop_details sd where category_id=?3 HAVING distance_m < 5000 ORDER  BY distance_m",nativeQuery = true)
    		List<RegisterEntity> getShopDetailsBasedOnLocationCatId(String lattitude, String longitude, String categoryId);

}
