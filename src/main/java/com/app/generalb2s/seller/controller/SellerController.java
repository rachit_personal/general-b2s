package com.app.generalb2s.seller.controller;

import java.util.List;

import javax.validation.Valid;

import com.app.generalb2s.buyer.service.OtpService;
import com.app.generalb2s.common.exception.DuplicateRegistration;
import com.app.generalb2s.common.exception.InvalidTokenException;
import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.seller.entity.ProductCategories;
import com.app.generalb2s.seller.helper.SaveObjectToAWSS3;
import com.app.generalb2s.seller.request.*;
import com.app.generalb2s.seller.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.app.generalb2s.common.validator.LoginValidation;
import com.app.generalb2s.seller.service.LoginServiceImpl;
import com.app.generalb2s.seller.service.RegisterServ;
import com.app.generalb2s.seller.service.SellerCategoriesService;
import com.app.generalb2s.seller.service.SellerProductDetailsSrvc;
import com.app.generalb2s.seller.service.SellerValidationService;
import com.app.generalb2s.seller.service.BuyerValidationService;

@RestController
@RequestMapping("/seller/")
public class SellerController {

	@Autowired
	private LoginServiceImpl login;
	
	@Autowired
	private LoginValidation validate;

	@Autowired
	private RegisterServ register;

	@Autowired
	private SellerProductDetailsSrvc sellerProduct;
	
	@Autowired
	private SellerValidationService val;

	@Autowired
	SaveObjectToAWSS3 saveObjectToAWSS3;
	
	@Autowired
	SellerCategoriesService sellerCat;

	
	@PostMapping(path = "shopLogin")
	public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request){
		
		if(validate.validation(request)) {
			return new ResponseEntity<>(login.login(request),HttpStatus.OK);
		} else {
			LoginResponse loginResponse= new LoginResponse();
			loginResponse.setServiceMessage("Login username or password is missing or not provided.");
			return new ResponseEntity<>(null,HttpStatus.OK);
		}
		
	}
	
	@PostMapping(path = "shopRegister")
	//return unique shop id
	public ResponseEntity<SellerRegisterResponse> register(@RequestBody @Valid RegisterRequest request) throws DuplicateRegistration{
		
		return new ResponseEntity<>(register.register(request),HttpStatus.OK);
		
	}
	
	@PostMapping(path="hello")
	public ResponseEntity<String> getHelloWorldResponse(){
        return new ResponseEntity<String>("This is a dummy api to test the connectivity",HttpStatus.OK);
	}
	
	@PostMapping(path="saveProductDetails")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<ProductDetailsResponse> saveProductDetails(@Valid @RequestBody ProductDetailsRequest productDetailsRequest) throws InvalidTokenException{
		if(!val.validate(productDetailsRequest.getTokenId(), productDetailsRequest.getDeviceId())) {
			throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
		}
		return new ResponseEntity<>(sellerProduct.saveSellerProductDetails(productDetailsRequest), HttpStatus.OK);
	}
	
	@PostMapping(path="getProductDetailsById")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<ProductDetails> getProductDetails(@Valid @RequestBody ProductDetailsByIdRequest productDetailsRequest) throws InvalidTokenException, ProductNotFoundException {
		if(!val.validate(productDetailsRequest.getTokenId(), productDetailsRequest.getDeviceId())) {
			throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
		}
		return new ResponseEntity<>(sellerProduct.getSellerProductDetails(productDetailsRequest), HttpStatus.OK);
	}

	@PutMapping(path="updateProductDetails/{productId}")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<ProductDetailsResponse> updateProductDetails(@PathVariable(value = "productId") String productId, @Valid @RequestBody ProductDetailsRequest productDetailsRequest) throws InvalidTokenException, ProductNotFoundException {
		if(!val.validate(productDetailsRequest.getTokenId(), productDetailsRequest.getDeviceId())) {
			throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
		}
			return new ResponseEntity<>(sellerProduct.updateSellerProductDetails(productDetailsRequest, productId), HttpStatus.OK);
	}


	@PostMapping(path="deleteProductDetails/")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<ProductDetailsResponse> updateProductDetails( @Valid @RequestBody ProductDetailsByIdRequest requestComponents) throws InvalidTokenException,ProductNotFoundException {
		if(!val.validate(requestComponents.getTokenId(), requestComponents.getDeviceId())) {
			throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
		}
		return new ResponseEntity<>(sellerProduct.deleteSellerProductDetails(requestComponents.getProductId()), HttpStatus.OK);
	}

	@PostMapping(path="getShopDetailsByShopId/")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<SellerDetailsResponse> getShopDetailsByShopId( @Valid @RequestBody ShopDetailsById shopDetailsById) throws InvalidTokenException,ProductNotFoundException {
		if(!val.validate(shopDetailsById.getTokenId(), shopDetailsById.getDeviceId())) {
			throw new InvalidTokenException("Invalid Token provided. Please check the token or try again.");
		}
		return new ResponseEntity<>(sellerProduct.getShopDetailsByShopId(shopDetailsById.getShopId()), HttpStatus.OK);
	}
	
	@GetMapping(path="getCategories/")
	// return productId and HTTP  if success 200 OK method or else return null product id and appropriate message
	public ResponseEntity<List<ProductCategories>> getCategories() throws InvalidTokenException,ProductNotFoundException {
		return new ResponseEntity<>(sellerCat.getCategories(), HttpStatus.OK);
	}
	
}
