package com.app.generalb2s.seller.request;

import com.app.generalb2s.common.model.ImageDetails;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class ProductDetailsRequest extends RequestComponents {

    @NonNull
    private String sellerId;
    @NonNull
    private String productName;
    @NonNull
    private String productDesc;
    @NonNull
    private String productBrand;
    @NonNull
    private String categoryId;

    private String subCategoryId;

    private String productPrice;

    private String isProductWishListed;

    private List<ImageDetails> productImage;

    public String getIsProductWishListed() {
        return isProductWishListed;
    }

    public void setIsProductWishListed(String isProductWishListed) {
        this.isProductWishListed = isProductWishListed;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public List<ImageDetails> getProductImage() {
        return productImage;
    }

    public void setProductImage(List<ImageDetails> productImage) {
        this.productImage = productImage;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }
}
