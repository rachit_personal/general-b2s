package com.app.generalb2s.buyer.response;

import com.app.generalb2s.seller.response.SellerDetailsResponse;

import java.util.List;

public class SellerDetailsByLocationResponse {

    List<SellerDetailsResponse>sellerDetailsResponses;

    public List<SellerDetailsResponse> getSellerDetailsResponses() {
        return sellerDetailsResponses;
    }

    public void setSellerDetailsResponses(List<SellerDetailsResponse> sellerDetailsResponses) {
        this.sellerDetailsResponses = sellerDetailsResponses;
    }
}
