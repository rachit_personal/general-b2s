package com.app.generalb2s.seller.request;

public class ShopDetailsById extends RequestComponents{

    private String shopId;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
