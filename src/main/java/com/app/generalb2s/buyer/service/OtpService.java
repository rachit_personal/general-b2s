package com.app.generalb2s.buyer.service;


import com.app.generalb2s.buyer.requestModel.BuyerRegisterRequest;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class OtpService {

    //cache based on username and OPT MAX 8
    private static final Integer EXPIRE_MINS = 15;

    private LoadingCache<String, Integer> otpCache;

    public OtpService() {
        super();
        otpCache = CacheBuilder.newBuilder().
                expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES).build(new CacheLoader<String, Integer>() {
            public Integer load(String key) {
                return 0;
            }
        });
    }

    //This method is used to push the opt number against Key. Rewrite the OTP if it exists
    //Using mobno is used as key
    public int generateOTP(String mobNo) {

        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        otpCache.put(mobNo, otp);
        return otp;
    }

    //This method is used to return the OPT number against Key->Key values is username
    //This method is used to return the OPT number against Key->Key values is username
    public int getOtp(String key) {
        try {
            return otpCache.get(key);
        } catch (Exception e) {
            return 0;
        }
    }

    //This method is used to clear the OTP cached already
    public void clearOTP(String key) {
        otpCache.invalidate(key);
    }

    public String sendSMSViaOpenSms(BuyerRegisterRequest buyerRegisterRequest, String apiKeys) {
        try {
            // Construct data
            String apiKey = "apikey=" + apiKeys;
            String message = "&message=" + generateOTP(buyerRegisterRequest.getMobNo()) + " is your OTP for registration. Please do not share the OTP with anyone else. Team SeerLink";
            String sender = "&sender=" + "SLINK";
            String numbers = "&numbers=" + buyerRegisterRequest.getMobNo();
            //String numbers = "&numbers=" + "919372003204";

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
            String data = apiKey + numbers + message + sender;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();
            System.out.println("Actual SMS send at"+new Timestamp(System.currentTimeMillis()) + stringBuffer.toString());
            return stringBuffer.toString();
        } catch (Exception e) {
            System.out.println("Error SMS " + e);
            return "Error " + e;
        }
    }
}