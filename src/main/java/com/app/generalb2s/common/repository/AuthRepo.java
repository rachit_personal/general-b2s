package com.app.generalb2s.common.repository;

import com.app.generalb2s.common.entity.AuthEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepo extends JpaRepository<AuthEntity, Long> {

    @Query(value = "SELECT s FROM AuthEntity s WHERE s.deviceId = ?1 and s.token = ?2 and s.status='ACTIVE' and"
            + " s.sessionExpiry > ?3")
    List<AuthEntity> checkAuth(String deviceId, String loginId, long time);

}
