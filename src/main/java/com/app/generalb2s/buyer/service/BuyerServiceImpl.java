package com.app.generalb2s.buyer.service;


import com.app.generalb2s.buyer.entity.BuyerAuthEntity;
import com.app.generalb2s.buyer.entity.BuyerDetailsEntity;
import com.app.generalb2s.buyer.entity.BuyerProductWishListEntity;
import com.app.generalb2s.buyer.helper.EntityPopulator;
import com.app.generalb2s.buyer.helper.MobileValidatorHelper;
import com.app.generalb2s.buyer.repository.BuyerAuthRepo;
import com.app.generalb2s.buyer.repository.BuyerDao;
import com.app.generalb2s.buyer.repository.BuyerProductWishListDao;
import com.app.generalb2s.buyer.requestModel.BuyerRegisterRequest;
import com.app.generalb2s.buyer.requestModel.MobileOtpValidationRequest;
import com.app.generalb2s.buyer.requestModel.ProductsDetailsLocationRequest;
import com.app.generalb2s.buyer.requestModel.WishListRequest;
import com.app.generalb2s.buyer.response.BuyerRegisterResponse;
import com.app.generalb2s.buyer.response.LoginResponse;
import com.app.generalb2s.buyer.response.MobileOTPValidationResponse;
import com.app.generalb2s.buyer.response.WishListResponse;
import com.app.generalb2s.common.exception.InvalidMobileNumber;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;

import java.util.Random;
import com.app.generalb2s.common.entity.AuthEntity;
import com.app.generalb2s.common.exception.AlreadyRegisteredException;
import com.app.generalb2s.common.exception.InvalidLocationDetailsException;
import com.app.generalb2s.common.exception.InvalidOtpException;
import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.common.repository.AuthRepo;
import com.app.generalb2s.seller.entity.ProductDetailsRequestEntity;
import com.app.generalb2s.seller.entity.RegisterEntity;
import com.app.generalb2s.seller.repository.SellerDetails;
import com.app.generalb2s.seller.repository.SellerProductDetailsDao;
import com.app.generalb2s.seller.request.LoginRequest;
import com.app.generalb2s.seller.request.ProductDetailsByIdRequest;
import com.app.generalb2s.seller.response.ProductDetails;
import com.app.generalb2s.seller.response.Response;
import com.app.generalb2s.seller.response.SellerDetailsResponse;
import com.app.generalb2s.seller.service.SellerProductDetailsSrvcImpl;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;

import static java.util.Objects.nonNull;

@Service
@Slf4j
public class BuyerServiceImpl implements BuyerService {


    @Value("${OTP.accountSID}")
    private static String ACCOUNT_SID;

    @Value("${OTP.authToken}")
    private static String AUTH_TOKEN;

    @Value("${OTP.apikey}")
    private static String apiKey="MzQ2YjczNTc1NzVhNGM2NTQ0NzAzODRjMzg0ZTYzNjQ=";

    @Autowired
    SellerDetails sellerDetails;

    @Autowired
    SellerProductDetailsSrvcImpl sellerProductDetailsSrvc;

    @Autowired
    BuyerDao buyerDao;

    @Autowired
    BuyerAuthRepo auth;

    @Autowired
    OtpService otpService;

    @Autowired
    SellerProductDetailsDao sellerProductDetailsDao;
    @Autowired
    BuyerProductWishListDao buyerProductWishListDao;

    public List<SellerDetailsResponse> getDataByLocation(ProductsDetailsLocationRequest productsDetailsLocationRequest) throws InvalidLocationDetailsException {
        List<RegisterEntity> registerEntities = sellerDetails.getShopDetailsBasedOnLocation(productsDetailsLocationRequest.getLattitude(), productsDetailsLocationRequest.getLongitude());
        List<SellerDetailsResponse> sellerDetailsResponses = new ArrayList<>();
        if (!registerEntities.isEmpty()) {
            for (RegisterEntity registerEntity : registerEntities) {
                SellerDetailsResponse sellerDetailsResponse = sellerProductDetailsSrvc.getShopDetailsByShopId(registerEntity.getShopId().toString());
                sellerDetailsResponses.add(sellerDetailsResponse);
            }
        } else {
        	throw new InvalidLocationDetailsException("No Shop Details Found for this Location");
        }
        return sellerDetailsResponses;
    }

    public List<SellerDetailsResponse> getDataByLocation(ProductsDetailsLocationRequest productsDetailsLocationRequest, String categoryId) {
        List<RegisterEntity> registerEntities = sellerDetails.getShopDetailsBasedOnLocationCatId(productsDetailsLocationRequest.getLattitude(), productsDetailsLocationRequest.getLongitude(), categoryId);
        List<SellerDetailsResponse> sellerDetailsResponses = new ArrayList<>();
        if (!registerEntities.isEmpty()) {
            for (RegisterEntity registerEntity : registerEntities) {
                SellerDetailsResponse sellerDetailsResponse = sellerProductDetailsSrvc.getShopDetailsByShopId(registerEntity.getShopId().toString());
                sellerDetailsResponses.add(sellerDetailsResponse);
            }
        }
        return sellerDetailsResponses;
    }


    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();

        BuyerDetailsEntity user = buyerDao.checkLogin(request.getLoginId());

        BuyerAuthEntity authEntity = null;
        if (nonNull(user) && user.getStatus().equalsIgnoreCase("Active") && request.getPassword().equalsIgnoreCase(user.getPassword())) {
            authEntity = generateToken(user.getBuyerId(), request);
        } else {
            response.setServiceStatus(Response.ERROR);
            response.setServiceMessage("Invalid Username/Password");
            return response;
        }

        response.setBuyerId(user.getBuyerId());
        response.setTokenId(authEntity.getTokenId());
        response.setServiceStatus(Response.SUCCESS);
        response.setServiceMessage("Login successfull");
        return response;
    }

    private BuyerAuthEntity generateToken(Long id, LoginRequest request) {

        BuyerAuthEntity token = new BuyerAuthEntity();
        token.setId(id);
        token.setDeviceId(request.getDeviceId());
        token.setKeepSignedIn(request.isKeepSignedInFlag());
        token.setTokenId(UUID.randomUUID().toString());
        token.setUserAgent(request.getUserAgent());
        Calendar cal = Calendar.getInstance();
        token.setAccessedTime(cal.getTimeInMillis());
        if (request.isKeepSignedInFlag()) {
            token.setSessionExpiry(cal.getTimeInMillis() + 31556952000L);
        } else {
            token.setSessionExpiry(cal.getTimeInMillis() + 1800000);
        }
        token.setStatus("ACTIVE");
        auth.save(token);
        return token;
    }

    @Override
    public BuyerRegisterResponse register(BuyerRegisterRequest request) throws InvalidOtpException, AlreadyRegisteredException {
    	
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        BuyerDetailsEntity res = modelMapper.map(request, BuyerDetailsEntity.class);
        res.setCreatedAt(Date.from(Instant.now()));
        res.setModifiedAt(Date.from(Instant.now()));
        res.setStatus("Active");
        try {
            res = buyerDao.save(res);

        } catch (Exception e) {
    		throw new AlreadyRegisteredException("I Think You have already registered with Mobile/Email");
		}

        BuyerRegisterResponse response = new BuyerRegisterResponse();
        response.setBuyerId(res.getBuyerId().toString());
        response.setServiceStatus(Response.SUCCESS);
        response.setServiceMessage("User registered");
        return response;

    }

    @Override
    public WishListResponse saveWishListProduct(WishListRequest wishListRequest) {
        WishListResponse wishListResponse = null;
        if (nonNull(wishListRequest.getProductId()) && !wishListRequest.getProductId().isEmpty()) {
            try {
                Optional<ProductDetailsRequestEntity> productDetailsRequestEntity = sellerProductDetailsDao.findById(Long.parseLong(wishListRequest.getProductId()));
                if (productDetailsRequestEntity.isPresent()) {
                    if (wishListRequest.getIsWishListed().equalsIgnoreCase("true")) {
                        BuyerProductWishListEntity productDetailsEntity = buyerProductWishListDao.save(EntityPopulator.getBuyerProductWishListEntityPopulator(wishListRequest));
                    } else {
                        buyerProductWishListDao.removeWishListedProduct(wishListRequest.getBuyerId(), wishListRequest.getProductId());
                    }

                    //wishListResponse.setProductId(productDetailsEntity.getProductId());
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
            	e.printStackTrace();
                wishListResponse.setResponseCode("500");
                wishListResponse.setMessage("Something went wrong in updating the product ids " + wishListRequest.getProductId() + " wishlist. Please try again.");
            }
        }
        return wishListResponse;
    }

    @Override
    public WishListResponse getWishListProduct(String buyerId) throws ProductNotFoundException {
        WishListResponse wishListResponse = new WishListResponse();
        if (!buyerId.isEmpty()) {
            List<BuyerProductWishListEntity> buyerProductWishListEntities = buyerProductWishListDao.findWishListProduct(buyerId);
            ProductDetailsByIdRequest productDetailsByIdRequest = new ProductDetailsByIdRequest();
            List<ProductDetails> productDetailsList = new ArrayList<>();
            buyerProductWishListEntities.forEach(x -> {
                productDetailsByIdRequest.setProductId(x.getProductId());
                ProductDetails productDetails = null;
                try {
                    productDetails = sellerProductDetailsSrvc.getSellerProductDetails(productDetailsByIdRequest);
                    productDetailsList.add(productDetails);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });


            wishListResponse.setProductDetails(productDetailsList);
            wishListResponse.setBuyerId(buyerId);
            wishListResponse.setMessage("Wish list for buyer id :" + buyerId + " returned successfully");
            wishListResponse.setResponseCode("200");
        }
        return wishListResponse;
    }


    public MobileOTPValidationResponse generateOTP(BuyerRegisterRequest buyerRegisterRequest) throws InvalidMobileNumber{
        Random random= new Random();
        MobileOTPValidationResponse mobileOTPValidationResponse= new MobileOTPValidationResponse();
        //Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
        try{

            buyerRegisterRequest.setMobNo( MobileValidatorHelper.mobValidatorHelper(buyerRegisterRequest.getMobNo()));
          /*  Message message = Message.creator(
                    new com.twilio.type.PhoneNumber(buyerMobId),
                    new com.twilio.type.PhoneNumber("+18455391563"),
                    "This is dummy OTP for General B2S applicaiton"+otpService.generateOTP(buyerMobId))
                    .create();*/
            String resp= otpService.sendSMSViaOpenSms(buyerRegisterRequest, apiKey);
            mobileOTPValidationResponse.setMessage(resp);
            mobileOTPValidationResponse.setMsgCode("200");
        }
        catch(InvalidMobileNumber e){
            throw e;
        }
        catch (Exception e){
            mobileOTPValidationResponse.setMessage("Generic exception. Something is wrong SMS provider. Please retry again or check for the recharge");
            mobileOTPValidationResponse.setMsgCode("500");
        }

        return mobileOTPValidationResponse;

    }


    public String validateOtp(MobileOtpValidationRequest mobileOtpValidationRequest){
        if (mobileOtpValidationRequest.getOtp().equalsIgnoreCase(otpService.getOtp(mobileOtpValidationRequest.getMobNo())+"")){
            otpService.clearOTP(mobileOtpValidationRequest.getMobNo());
            return "OTP validation successfull";

        }
        else {
            return "Something wrong with the OTP. Please generate the new OTP again";
        }
    }

    public String validateExistingBuyer(String mobNo) throws InvalidMobileNumber {

        String buyerMobId= MobileValidatorHelper.mobValidatorHelper(mobNo);
        BuyerDetailsEntity user = buyerDao.checkLogin(buyerMobId);
        if (nonNull(user) && nonNull(user.getBuyerId())){
            return "Buyer already present. Please login instead of Sign Up";
        }
        else
        return "New buyer. Please sign up.";
    }

}
