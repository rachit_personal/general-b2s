package com.app.generalb2s.seller.response;

public class SellerRegisterResponse extends ServiceResponse {

    private String sellerId;
    private String message;

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
