package com.app.generalb2s.seller.service;

import com.app.generalb2s.common.entity.AuthEntity;
import com.app.generalb2s.seller.entity.RegisterEntity;
import com.app.generalb2s.common.repository.AuthRepo;
import com.app.generalb2s.seller.repository.SellerDetails;
import com.app.generalb2s.seller.request.LoginRequest;
import com.app.generalb2s.seller.response.LoginResponse;
import com.app.generalb2s.seller.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Service
public class LoginServiceImpl {

    @Autowired
    private SellerDetails seller;

    @Autowired
    AuthRepo authR;

    public LoginResponse login(LoginRequest request) {
        LoginResponse response = new LoginResponse();

        RegisterEntity user = seller.checkLogin(request.getLoginId());

        AuthEntity authEntity = null;
        if (nonNull(user) && user.getStatus().equalsIgnoreCase("Active") && request.getPassword().equalsIgnoreCase(user.getPassword())) {
            authEntity = generateToken(user.getShopId(), request);
        } else {
            response.setServiceStatus(Response.ERROR);
            response.setServiceMessage("Invalid Username/Password");
            return response;
        }

        response.setShopId(user.getShopId());
        response.setTokenId(authEntity.getToken());
        response.setServiceStatus(Response.SUCCESS);
        response.setServiceMessage("Login successfull");
        return response;
    }

    private AuthEntity generateToken(Long id, LoginRequest request) {

        AuthEntity token = new AuthEntity();
        token.setId(id);
        token.setDeviceId(request.getDeviceId());
        token.setKeepSignedIn(request.isKeepSignedInFlag());
        token.setToken(UUID.randomUUID().toString());
        token.setUserAgent(request.getUserAgent());
        Calendar cal = Calendar.getInstance();
        token.setAccessedTime(cal.getTimeInMillis());
        if (request.isKeepSignedInFlag()) {
            token.setSessionExpiry(cal.getTimeInMillis() + 31556952000L);
        } else {
            token.setSessionExpiry(cal.getTimeInMillis() + 1800000);
        }
        token.setStatus("ACTIVE");
        authR.save(token);
        return token;
    }
}
