package com.app.generalb2s.buyer.helper;

import com.app.generalb2s.buyer.entity.BuyerProductWishListEntity;
import com.app.generalb2s.buyer.requestModel.WishListRequest;

public class EntityPopulator {


    public static BuyerProductWishListEntity getBuyerProductWishListEntityPopulator(WishListRequest wishListRequest){
        BuyerProductWishListEntity buyerProductWishListEntity= new BuyerProductWishListEntity();
        buyerProductWishListEntity.setBuyerId(wishListRequest.getBuyerId());
        buyerProductWishListEntity.setProductId(wishListRequest.getProductId());
        return buyerProductWishListEntity;
    }
}
