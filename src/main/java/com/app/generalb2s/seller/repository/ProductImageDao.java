package com.app.generalb2s.seller.repository;

import com.app.generalb2s.seller.entity.ProductImageEntity;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public interface ProductImageDao extends JpaRepository<ProductImageEntity, Integer> {

    @Query(value = "Delete from ProductImageEntity i where i.productId = ?1")
    @Modifying
    public void deleteImageBySellerId(Long productId);
}
