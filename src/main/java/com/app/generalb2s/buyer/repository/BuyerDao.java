package com.app.generalb2s.buyer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.generalb2s.buyer.entity.BuyerDetailsEntity;

@Repository
public interface BuyerDao extends JpaRepository<BuyerDetailsEntity, Long> {

	@Query(value = "SELECT r FROM BuyerDetailsEntity r WHERE r.mobNo = ?1")
	BuyerDetailsEntity checkLogin(String loginId);

}
