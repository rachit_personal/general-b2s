package com.app.generalb2s.common.exception;

public class AlreadyRegisteredException extends Exception {

	public AlreadyRegisteredException(String msg) {
		super(msg);
	}
	
}
