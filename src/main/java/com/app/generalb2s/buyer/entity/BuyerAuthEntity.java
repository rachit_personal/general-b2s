package com.app.generalb2s.buyer.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "buyer_auth")
public class BuyerAuthEntity {


        @Id
        private Long id;
        private String deviceId;
        private boolean keepSignedIn;
        private String tokenId;
        private String userAgent;
        private long accessedTime;
        private Date created_at;
        private long sessionExpiry;
        private String status;


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public long getSessionExpiry() {
            return sessionExpiry;
        }

        public void setSessionExpiry(long sessionExpiry) {
            this.sessionExpiry = sessionExpiry;
        }

        public long getAccessedTime() {
            return accessedTime;
        }

        public void setAccessedTime(long accessedTime) {
            this.accessedTime = accessedTime;
        }

        public Date getCreated_at() {
            return created_at;
        }

        public void setCreated_at(Date created_at) {
            this.created_at = created_at;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public boolean isKeepSignedIn() {
            return keepSignedIn;
        }

        public void setKeepSignedIn(boolean keepSignedIn) {
            this.keepSignedIn = keepSignedIn;
        }



        public String getTokenId() {
			return tokenId;
		}

		public void setTokenId(String tokenId) {
			this.tokenId = tokenId;
		}

		public String getUserAgent() {
            return userAgent;
        }

        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }



}
