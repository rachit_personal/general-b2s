package com.app.generalb2s.seller.entity;

import lombok.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product_details")
public class ProductDetailsRequestEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "productid")
    private Long productId;

    @NonNull
    @Column(name = "product_name")
    private String productName;

    @NonNull
    @Column
    private String sellerId;

    @Column(name = "product_desc")
    @NonNull
    private String productDesc;

    @Column(name = "product_brand")
    @NonNull
    private String productBrand;

    @Column(name = "category_id")
    @NonNull
    private String categoryId;

    @NonNull
    @Column(name = "product_price")
    private String productPrice;

    @OneToMany(mappedBy = "productId", cascade = CascadeType.ALL)
    private List<ProductImageEntity> pImage;


    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }


    private String subCategoryId;

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<ProductImageEntity> getpImage() {
        return pImage;
    }

    public void setpImage(List<ProductImageEntity> pImage) {
        this.pImage = pImage;
    }

    @Override
    public String toString() {
        return "ProductDetailsRequestEntity [productId=" + productId + ", productName=" + productName + ", sellerId="
                + sellerId + ", productDesc=" + productDesc + ", productBrand=" + productBrand + ", categoryId="
                + categoryId + ", productPrice=" + productPrice + ", pImage=" + pImage + ", subCategoryId="
                + subCategoryId + "]";
    }


}
