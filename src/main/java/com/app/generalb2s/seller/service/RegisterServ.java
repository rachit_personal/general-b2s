package com.app.generalb2s.seller.service;

import com.app.generalb2s.common.exception.DuplicateRegistration;
import com.app.generalb2s.seller.entity.RegisterEntity;
import com.app.generalb2s.seller.repository.RegisterDao;
import com.app.generalb2s.seller.request.RegisterRequest;
import com.app.generalb2s.seller.response.Response;
import com.app.generalb2s.seller.response.SellerRegisterResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;

@Service
public class RegisterServ {

    @Autowired
    RegisterDao register;

    public SellerRegisterResponse register(RegisterRequest request) throws DuplicateRegistration {
        ModelMapper modelMapper = new ModelMapper();
        RegisterEntity res = modelMapper.map(request, RegisterEntity.class);
        res.setCreatedAt(Date.from(Instant.now()));
        res.setModifiedAt(Date.from(Instant.now()));
        try {
        res = register.save(res);
        } catch (DataIntegrityViolationException e) {
			throw new DuplicateRegistration("Shop email/mobile Already Registerd");
		}
        
        		
        SellerRegisterResponse response = new SellerRegisterResponse();
        response.setSellerId(res.getShopId().toString());
        response.setServiceStatus(Response.SUCCESS);
        response.setServiceMessage("User registered");
        return response;

    }

}
