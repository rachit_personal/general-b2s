package com.app.generalb2s.common.exception;

public class InvalidOtpException extends Exception {

	public InvalidOtpException(String msg) {
		super(msg);
	}
}
