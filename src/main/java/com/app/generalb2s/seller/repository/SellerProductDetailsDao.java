package com.app.generalb2s.seller.repository;


import com.app.generalb2s.seller.entity.ProductDetailsRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SellerProductDetailsDao extends JpaRepository<ProductDetailsRequestEntity, Long> {

    @Query(value = "select s from ProductDetailsRequestEntity s where s.sellerId=?1")
    List<ProductDetailsRequestEntity> getProductDetailsByShopId(String shopId);
}
