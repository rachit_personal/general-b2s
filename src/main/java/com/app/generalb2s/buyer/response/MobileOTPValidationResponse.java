package com.app.generalb2s.buyer.response;

public class MobileOTPValidationResponse {
    private String message;
    private String msgCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }
}
