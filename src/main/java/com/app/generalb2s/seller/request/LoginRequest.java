package com.app.generalb2s.seller.request;

import lombok.NonNull;

public class LoginRequest {
    @NonNull
    private String loginId;
    @NonNull
    private String deviceId;
    @NonNull
    private boolean keepSignedInFlag;
    @NonNull
    private String userAgent;
    @NonNull
    private String version;
    @NonNull
    private String password;


    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isKeepSignedInFlag() {
        return keepSignedInFlag;
    }

    public void setKeepSignedInFlag(boolean keepSignedInFlag) {
        this.keepSignedInFlag = keepSignedInFlag;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
