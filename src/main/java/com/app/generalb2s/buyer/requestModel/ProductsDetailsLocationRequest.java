package com.app.generalb2s.buyer.requestModel;

public class ProductsDetailsLocationRequest extends RequestComponents{

    private String lattitude;
    private String longitude;

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
