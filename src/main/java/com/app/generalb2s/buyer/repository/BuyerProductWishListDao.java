package com.app.generalb2s.buyer.repository;

import com.app.generalb2s.buyer.entity.BuyerProductWishListEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface BuyerProductWishListDao extends JpaRepository<BuyerProductWishListEntity,Long> {

    @Query(value = "select s from BuyerProductWishListEntity s where s.buyerId=?1 ")
    List<BuyerProductWishListEntity> findWishListProduct(String buyerId);

    @Query(value = "delete from BuyerProductWishListEntity s where s.buyerId=?1 and s.productId= ?2")
    @Modifying
    void removeWishListedProduct(String buyerId, String productId);
}
