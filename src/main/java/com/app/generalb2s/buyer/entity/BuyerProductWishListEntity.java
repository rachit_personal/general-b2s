package com.app.generalb2s.buyer.entity;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
/**
 * This class is being created for maintaining buyer and product details like wishlist,
 * list of products purchased by particular buyer
 * list of product returned by buyer etc
 **/

@Entity
public class BuyerProductWishListEntity {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long wishListId;

    private String buyerId;

    //@OneToMany(mappedBy = "buyerId", cascade = CascadeType.ALL)
    private String productId;

    public Long getWishListId() {
        return wishListId;
    }

    public void setWishListId(Long wishListId) {
        this.wishListId = wishListId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
