package com.app.generalb2s.buyer.requestModel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.app.generalb2s.buyer.response.MobileOTPValidationResponse;
import org.springframework.lang.NonNull;


public class BuyerRegisterRequest {

	@NotNull(message = "name cannot be empty")
    private String name;
	
    @NotNull(message = "lat cannot be empty")
    private String latitude;
    
    @NotNull(message = "long cannot be empty")
    private String longitude;
    
    private String emailId;

    @NotNull(message = "phone cannot be empty")
    @Pattern(regexp = "^(\\+91[\\-\\s]?)?[0]?(91)?[789]\\d{9}$" , message = "invalid phone number... format +919876543210")
    private String mobNo;

    @NotNull(message = "password cannot be empty")
    private String password;
    
    @NotNull(message = "uh..oh.. bhai otp bhi chahiye")
    private String otp;
    
    
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



}
