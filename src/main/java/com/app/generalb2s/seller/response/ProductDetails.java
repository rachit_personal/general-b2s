package com.app.generalb2s.seller.response;

import com.app.generalb2s.common.model.ImageDetails;

import java.util.List;

public class ProductDetails extends ServiceResponse {

    private Long productId;

    private String productName;

    private String sellerId;

    private String productDesc;

    private String productBrand;

    private String categoryId;

    private String productPrice;

    private List<ImageDetails> productImage;


    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }


    private String subCategoryId;

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<ImageDetails> getProductImage() {
        return productImage;
    }

    public void setProductImage(List<ImageDetails> productImage) {
        this.productImage = productImage;
    }

}
