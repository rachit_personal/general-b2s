package com.app.generalb2s.seller.request;

import org.springframework.lang.NonNull;


public class ProductDetailsByIdRequest extends RequestComponents {

    @NonNull
    private String productId;


    public ProductDetailsByIdRequest() {
        super();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


}
