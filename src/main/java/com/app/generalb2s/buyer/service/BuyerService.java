package com.app.generalb2s.buyer.service;

import com.app.generalb2s.buyer.requestModel.BuyerRegisterRequest;
import com.app.generalb2s.buyer.requestModel.MobileOtpValidationRequest;
import com.app.generalb2s.buyer.requestModel.ProductsDetailsLocationRequest;
import com.app.generalb2s.buyer.requestModel.WishListRequest;
import com.app.generalb2s.buyer.response.BuyerRegisterResponse;
import com.app.generalb2s.buyer.response.LoginResponse;
import com.app.generalb2s.buyer.response.MobileOTPValidationResponse;
import com.app.generalb2s.buyer.response.WishListResponse;
import com.app.generalb2s.common.exception.AlreadyRegisteredException;
import com.app.generalb2s.common.exception.InvalidLocationDetailsException;
import com.app.generalb2s.common.exception.InvalidMobileNumber;
import com.app.generalb2s.common.exception.InvalidOtpException;
import com.app.generalb2s.common.exception.ProductNotFoundException;
import com.app.generalb2s.seller.request.LoginRequest;
import com.app.generalb2s.seller.response.SellerDetailsResponse;

import java.util.List;

public interface BuyerService {

    public List<SellerDetailsResponse> getDataByLocation(ProductsDetailsLocationRequest productsDetailsLocationRequest) throws InvalidLocationDetailsException ;

    public List<SellerDetailsResponse> getDataByLocation(ProductsDetailsLocationRequest productsDetailsLocationRequest, String categoryId) throws InvalidLocationDetailsException ;

    public LoginResponse login(LoginRequest request);

    public BuyerRegisterResponse register(BuyerRegisterRequest request) throws InvalidOtpException,AlreadyRegisteredException;


    public WishListResponse saveWishListProduct(WishListRequest wishListRequest) throws ProductNotFoundException;

    WishListResponse getWishListProduct(String buyerId) throws ProductNotFoundException;


    public MobileOTPValidationResponse generateOTP(BuyerRegisterRequest buyerRegisterRequest) throws InvalidMobileNumber;

    Object validateOtp(MobileOtpValidationRequest mobileOtpValidationRequest);

    String validateExistingBuyer(String mobNo) throws InvalidMobileNumber;
}
