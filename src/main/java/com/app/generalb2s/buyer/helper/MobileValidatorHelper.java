package com.app.generalb2s.buyer.helper;

import com.app.generalb2s.common.exception.InvalidMobileNumber;

import static java.util.Objects.nonNull;

public class MobileValidatorHelper {

    private static Integer STANDARD_MOBILE_NO_LENGTH=10;

    public static String mobValidatorHelper(String mobNo) throws InvalidMobileNumber {
        if(nonNull(mobNo) && !mobNo.isEmpty() && mobNo.length()==STANDARD_MOBILE_NO_LENGTH){
            if(mobNo.charAt(0) =='0' || mobNo.substring(0, 2).equalsIgnoreCase("+91")){
                throw new InvalidMobileNumber("Please dont append country code or 0 to the mobile number");
            }
        }
        return mobNo;
    }
}
