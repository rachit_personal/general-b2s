package com.app.generalb2s.seller.response;

public class LoginResponse extends ServiceResponse {

    private String tokenId;
    private Long shopId;
       
    public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }


}
